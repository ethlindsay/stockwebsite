var buttonClicked = 0;

var fetchData = function(numDays) {
	if (buttonClicked != 0) {
		document.getElementById('data-table').remove();
	}
	var stock = document.getElementById("drop-down").value;
	if (numDays <= 100) {
		fetch("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+stock+"&apikey=WBNSB6B7DTPALTDZ")
		.then(res => res.json())
		.then(function(response) {
			var priceList = response["Time Series (Daily)"];
			var refinedList = [];
			var dayCount = 0;
			for (var day in priceList) {
				if (dayCount < numDays) {
					var elem = [day, (parseFloat(priceList[day]["4. close"]).toFixed(2))];
					refinedList.push(elem);
					}
					dayCount++;
				}
			createTable(refinedList, numDays);
			drawChart(refinedList.reverse());
			buttonClicked++;
			}
		);
	}
	else {
		fetch("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+stock+"&apikey=WBNSB6B7DTPALTDZ&outputsize=full")
		.then(res => res.json())
		.then(function(response) {
			var priceList = response["Time Series (Daily)"];
			var refinedList = [];
			var dayCount = 0;
			for (var day in priceList) {
				if (dayCount < numDays) {
					var elem = [day, (parseFloat(priceList[day]["4. close"]).toFixed(2))];
					refinedList.push(elem);
					}
					dayCount++;
				}
			var stringJ = JSON.stringify(response);
			createTable(refinedList, numDays);
			drawChart(refinedList.reverse());
			buttonClicked++;
			}
		);
	}
}

var createTable = function(data, numDays) {
	table = document.createElement('table');
	table.setAttribute("id","data-table");
	row = document.createElement('tr');
	dateHeader = document.createElement('th');
	dateHeader.textContent = "Date:";
	priceHeader = document.createElement('th');
	priceHeader.textContent = "Price:";
	row.appendChild(dateHeader);
	row.appendChild(priceHeader);
	table.appendChild(row);
	table.setAttribute("Class","table text-black stock-table");
	for (var day in data) {
		row = document.createElement('tr');
		column1 = document.createElement('td');
		var currDate = new Date(data[day][0]);
		var cDay = currDate.getDate();
		var month = currDate.getMonth()+1;
		var year = currDate.getYear()-100+2000;
		column1.textContent = month+"/"+cDay+"/"+year;
		column2 = document.createElement('td');
		column2.textContent = data[day][1];
		row.appendChild(column1);
		row.appendChild(column2);
		table.appendChild(row);
	}
	document.getElementById("price-list-div").setAttribute("overflow","scroll");
	document.getElementById("price-list-div").setAttribute("class","bg-white");
	document.getElementById("price-list-div").appendChild(table);
}

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart(refinedList) {
	if (typeof refinedList == "object") {
	var list = refinedList;
	for (var i = 0; i < list.length; i++) {
		var tempDate = refinedList[i][0];
		var tempPrice = refinedList[i][1];
		tempPrice = parseFloat(tempPrice);
		refinedList[i][0] = new Date(tempDate);
		refinedList[i][1] = tempPrice;
		}
	
	var data = new google.visualization.DataTable();
	data.addColumn('date', 'Date');
	data.addColumn('number','Stock Price');
	
	data.addRows(refinedList);
	
	var options = {
		title: 'Stock Chart',
		hAxis: {
		format: 'M/d/yy'}
	};
	
	var chart = new google.visualization.LineChart(document.getElementById('chart-div'));
	
	chart.draw(data, options);
	}
}


